import React, { useRef, useState, useEffect } from "react";
import "./App.css";

import data from "./data";

const minMaxDates = orders => {
  const minDate = Math.min(...orders.map(x => new Date(x.from)));
  const maxDate = Math.max(...orders.map(x => new Date(x.to)));

  return { minDate: new Date(minDate), maxDate: new Date(maxDate) };
};

var addHours = function (input, hours) {
  input.setHours(input.getHours() + hours);
  return input;
};

const datesDifferenceInMinutes = (date1, date2) => {
  let diff = Math.abs(new Date(date1) - new Date(date2));
  let diffInMinutes = Math.floor((diff / 1000) / 60);
  return diffInMinutes;
};

const formatDate = (date) => {
  const dayOfMonth = date.getDate().toString().padStart(2, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');
  return `${month}.${dayOfMonth} ${hours}:${minutes}`;
};

const makeSpacer = (from, to, key) => {
  const spacerLength = datesDifferenceInMinutes(from, to);
  const spacerWidth = `${spacerLength}px`;
  return (<div key={key} className="order-item spacer" style={{ width: spacerWidth }}></div>);
};

// 4x60 = 240 pixel - 1 perc 1 pixel
export default (props) => {
  const [truckFilter, setTruckFilter] = useState('');
  const [truckData, setTruckData] = useState({
    trucks: [],
    orders: []
  });
  let periodRef = useRef();
  let truckNamesRef = useRef();
  const { minDate, maxDate } = minMaxDates(data.orders);

  useEffect(() => {
    // set initial state
    setTruckData(data);
  }, []);

  useEffect(() => {
    const filterChanged = () => {
      // copy object
      let trucksData = { ...data };
      trucksData.trucks = trucksData.trucks
        .filter(x => x.name.toLowerCase().indexOf(truckFilter.toLowerCase()) > -1);
      setTruckData(trucksData);
    };
    filterChanged();
  }, [truckFilter]);

  const getPeriods = () => {
    let children = [];
    let paddedMaxDate = addHours(maxDate, 4);
    for (let i = new Date(minDate); i < paddedMaxDate; i = addHours(i, 4)) {
      children.push(
        <div key={i} className="period">
          <div className="date">{`${formatDate(i)}`}</div>
        </div>
      );
    }

    return children;
  };

  const getTruckOrders = () => {
    let rows = [];
    for (var j = 0; j < truckData.trucks.length; j++) {
      let truck = truckData.trucks[j];

      let truckAssignments = [];
      // Assuming orders are ordered by date per truck
      // Kamionnak mindig van legalabb egy utja, nincs olyan hogy csak ures jarata van
      for (var i = 0; i < truck.assignedOrderId.length; i++) {
        const orderId = truck.assignedOrderId[i];
        const order = data.orders.find(x => x.id === orderId);
        if (i === 0) {
          // a -4óra eltolás a dátum beosztások miatt kell, az is 4órával hamrabb kezdődik
          truckAssignments.push(makeSpacer(addHours(new Date(minDate), -4), order.from, "first" + orderId));
        }

        const usageDiff = datesDifferenceInMinutes(order.from, order.to);
        const usageWidth = `${usageDiff}px`;
        truckAssignments.push(
          <div key={orderId} className="order-item order" style={{ width: usageWidth }}>
            <div className="order-content">
              <div className="green-line"></div>
              <div>{orderId}</div>
            </div>
          </div>
        );

        // ha van tobb utja a kamionnak akkor vonal a kovetkezo hasznalat dobozig
        if (i + 1 < truck.assignedOrderId.length) {
          const nextOrderId = truck.assignedOrderId[i + 1];
          const nextOrder = data.orders.find(x => x.id === nextOrderId);
          truckAssignments.push(makeSpacer(order.to, nextOrder.from, orderId + nextOrderId));
        }
      }
      rows.push(
        <div key={truck.name} className="items">
          {truckAssignments}
          <div className="spacer"></div>
        </div>
      );
    }

    // returning array of arrays
    return rows;
  }

  const getTruckNames = () => truckData.trucks.map((item) => (
    <div key={item.name} className="name">{`${item.name}`}</div>
  ));

  const onTimeLineScroll = (event) => {
    let { target: { scrollLeft, scrollTop } } = event;
    periodRef.current.scroll(scrollLeft, 0);
    truckNamesRef.current.scroll(0, scrollTop);
  };

  const truckFilterChanged = (event) => {
    setTruckFilter(event.target.value);
  };

  return (
    <div className="wrapper">
      <div className="searchbox-box">
        <label htmlFor="search">Truck filter</label>
        <input onChange={truckFilterChanged} type="text" id="search" />
      </div>
      <div className="diagram-container">
        <div className="truck-names-wrapper">
          <div style={{ height: '63px' }}></div>
          <div className="truck-names" ref={truckNamesRef}>
            {getTruckNames()}
          </div>
        </div>
        <div className="period-container">
          <div className="period-wrapper" ref={periodRef}>
            <div className="period" style={{ width: '120px' }}>
            </div>
            {getPeriods()}
          </div>
          <div className="timeline" onScroll={onTimeLineScroll}>
            <div className="items-wrapper">
              {getTruckOrders()}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
